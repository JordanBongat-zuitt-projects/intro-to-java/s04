package com.bongat.lib;

public class Car {
    // Properties
    // Syntax:
    // accessModifier dataType identifier;
    private String name;
    private String brand;
    private int yearOfMake;

    // Constructors
    // Is merely a special purpose function of a class.
    // Syntax:
    // accessModifier functionName(parameters) {}

    // Empty constructor
    public Car() {}

    // Parameterized constructor
    public Car(String name, String brand, int yearOfMake) {
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
    }

    // Getters & Setters
    // They are a set of special-purpose function of a class.
    // They can only retrieve and update values of the properties.
    // Syntax:
    // accessModifier returnDataType functionName(params) {}

    public String getName() {
        return name;
    };

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return this.brand = brand;
    }

    // Methods
    // Functions that represents the class' behaviour.
    // Syntax:
    // accessModifier returnDataType functionName(params) {}

    public void drive() {
        System.out.println("Driving the " + this.name);
    }
}
