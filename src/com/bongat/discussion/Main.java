package com.bongat.discussion;

import abstraction.Person;
import com.bongat.lib.Car;
import inheritance.Dog;
import polymorphism.Child;
import polymorphism.Parent;
import polymorphism.StaticPolymorphism;

public class Main {

    public static void main(String[] args) {
        // Creating a new instance of Car class.
        // className identifier = new className();
        Car firstCar = new Car("M4 Coupe", "BMW", 2015);
        Car secondCar = new Car("Adventure", "Mitsubishi", 2000);

        firstCar.drive();
        secondCar.drive();

        System.out.println("I have a new car, the " + firstCar.getName());
        System.out.println("I have a new car, the " + secondCar.getName());


        firstCar.setName("M5 Sedan");

        System.out.println("I swap my new car to " + firstCar.getName());

        System.out.println(firstCar.getBrand());
//        System.out.println(firstCar.brand);

        Dog firstDog = new Dog();
        System.out.println(firstDog.getBreed());

        firstDog.speak();

        Dog secondDog = new Dog("Pooh", "Yellow", "German Shepherd");
        System.out.println(secondDog.getBreed());
        secondDog.speak();

        Person firstPerson = new Person();
        firstPerson.sleep();
        firstPerson.run();

        StaticPolymorphism newStatic = new StaticPolymorphism();
        System.out.println(newStatic.sum(1, 2));
        System.out.println(newStatic.sum(1, 2, 3));
        System.out.println(newStatic.sum(1.5, 2.2));

        Parent newParent = new Parent();
        Child newChild = new Child();
        newParent.speak();
        newChild.speak();
    }
}
