package abstraction;

public class Person implements Actions {

    public void sleep() {
        System.out.println("zzZZZ...");
    }

    public void run() {
        System.out.println("Running...");
    }
}
