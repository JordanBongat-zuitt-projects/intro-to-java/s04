package abstraction;

public interface Actions {
    public void sleep();
    public void run();
}
