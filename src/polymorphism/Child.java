package polymorphism;

public class Child extends Parent {

    @Override
    public void speak() {
        System.out.println("I am a mere child");
    }
}
