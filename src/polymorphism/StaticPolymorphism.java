package polymorphism;

public class StaticPolymorphism {

    public int sum(int a, int b) {
        return a + b;
    }

    public int sum(int a, int b, int c) {
        return a + b + b;
    }

    public double sum(double a, double b) {
        return a + b;
    }
}
