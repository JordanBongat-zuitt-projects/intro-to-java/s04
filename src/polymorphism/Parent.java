package polymorphism;

public class Parent {

    public void speak() {
        System.out.println("I am the parent");
    }
}
