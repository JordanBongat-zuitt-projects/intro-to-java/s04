package inheritance;

public class Dog extends Animal{
    // Properties
    private String breed;

    // Default (empty) constructor
    public Dog() {
        super(); // Animal constructor
        this.breed = "Aspin";
    }

    // Parameterized constructor
    public Dog(String name, String color, String breed) {
        super(name, color); // Animal constructor
        this.breed = breed;
    }

    // Getters & Setters
    public String getBreed() {
        return breed;
    }

    // Methods
    public void speak() {
        System.out.println("Bark!");
    }
}
