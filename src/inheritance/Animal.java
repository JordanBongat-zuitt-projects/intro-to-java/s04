package inheritance;

public class Animal {
    // Properties
    private String name;
    private String color;

    // Constructor
    public Animal() {} // default constructor

    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    // Getters
    public String getName() {
        return name;
    }
    public String getColor() {
        return color;
    }

    // Setters
    public void setName(String newName) {
        this.name = newName;
    }
    public void setColor(String newColor) {
        this.color = newColor;
    }

    // Methods
    public void call() {
        System.out.println("Hi, My name is " + this.name);
    }
}
